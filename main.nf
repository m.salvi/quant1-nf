#!/usr/bin/env nextflow

/*
========================================================================================
                3' RNA-Seq 
========================================================================================
 @#### Authors
 Marcello Salvi, Lucio di Filippo, Chiara Colantuomo
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
 - 1. : Preprocessing
 	- 1.1: FastQC - for raw sequencing reads quality control
 	- 1.2: Trimmomatic  - raw sequence trimming
 - 2. : Mapping
 	- 2.1 : STAR - Mapping to human genome
    - 2.2 : Samtools - SAM and BAM files processing and stats
 - 3. : counts:
 	- 3.1 : Features Count count
 - 4. : Stats
  - 4.1 : MultiQC - Final html report with stats
 - 5. : Output Description HTML
 ----------------------------------------------------------------------------------------
*/

def helpMessage() {
    log.info"""
    =========================================
    =========================================
    Usage:
    The typical command for running the pipeline is as follows:
    Mandatory arguments:
      --reads                        Path to input data (must be surrounded with quotes).
      --gtf_dir                      Path to genome fasta index
     
    Options:
      --singleEnd                   Specifies that the input is single end reads
     
    Trimming options
      --notrim                      Specifying --notrim will skip the adapter trimming step.
      --saveTrimmed                 Save the trimmed Fastq files in the the Results directory.
      --trimmomatic_adapters_file   Adapters index for adapter removal
      --trimmomatic_adapters_parameters Trimming parameters for adapters. <seed mismatches>:<palindrome clip threshold>:<simple clip threshold>. Default 2:30:10
      --trimmomatic_window_length   Window size. Default 4
      --trimmomatic_window_value    Window average quality requiered. Default 20
      --trimmomatic_mininum_length  Minimum length of reads
    Other options:
      --save_unmapped_host          Save the reads that didn't map to host genome
      --outdir                      The output directory where the results will be saved

    """.stripIndent()
}

/*
 * SET UP CONFIGURATION VARIABLES
 */
params.help = false


// Pipeline version
version = '1.0'

// Show help emssage
if (params.help){
    helpMessage()
    exit 0
}

/*
 * Default and custom value for configurable variables
 */



        
// Stage config files
params.multiqc_config = "${baseDir}/multiqc_config.yaml"

if (params.multiqc_config){
    multiqc_config = file(params.multiqc_config)
}

params.doc_output = "${params.outdir}/../DOC/"

if (params.doc_output){
    doc_output = params.doc_output
}

ch_output_docs = Channel.fromPath("$baseDir/docs/output.md")





    Channel
        .fromPath(params.idx_dir)
        .ifEmpty { exit 1, "Fasta file not found: ${params.idx_dir}."}
        .set{genome_idx_dir }


// SingleEnd option
params.singleEnd = true

// Validate  mandatory inputs
params.reads = false
if (! params.reads ) exit 1, "Missing reads: $params.reads. Specify path with --reads"

/*
 * Create channel for input files
 */

// Create channel for input reads.
Channel
    .fromFilePairs( params.reads, size: params.singleEnd ? 1 : 2 )
    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}\nIf this is single-end data, please specify --singleEnd on the command line." }
    .into { raw_reads_fastqc; raw_reads_trimming }

// Create channel for reference index

if( params.gtf_dir ){
    Channel
        .fromPath(params.gtf_dir)
        .ifEmpty { exit 1, "GTF index not found: ${params.gtf_dir}" }
        .set { gtf_featureCounts}
}
if (params.trimmomatic_adapters_file) { truseq = file(params.trimmomatic_adapters_file, checkIfExists: true) } else { exit 1, "TruSeq not found!" }


// Header log info
log.info "========================================="
log.info " QuantSeq analysis v${version}"
log.info "========================================="
def summary = [:]
summary['Reads']               = params.reads
summary['Data Type']           = params.singleEnd ? 'Single-End' : 'Paired-End'
summary['Current home']        = "$HOME"
summary['Current user']        = "$USER"
summary['Current path']        = "$PWD"
summary['Working dir']         = workflow.workDir
summary['Output dir']          = params.outdir
summary['Script dir']          = workflow.projectDir
summary['Save Unmapped']        = params.save_unmapped_host
summary['Save Trimmed']        = params.saveTrimmed
if( params.notrim ){
    summary['Trimming Step'] = 'Skipped'
} else {
    summary['Trimmomatic adapters file'] = params.trimmomatic_adapters_file
    summary['Trimmomatic adapters parameters'] = params.trimmomatic_adapters_parameters
    summary["Trimmomatic window length"] = params.trimmomatic_window_length
    summary["Trimmomatic window value"] = params.trimmomatic_window_value
    summary["Trimmomatic minimum length"] = params.trimmomatic_mininum_length
}
summary['Config Profile'] = workflow.profile
log.info summary.collect { k,v -> "${k.padRight(21)}: $v" }.join("\n")
log.info "===================================="

// Check that Nextflow version is up to date enough
// try / throw / catch works for NF versions < 0.25 when this was implemented
nf_required_version = '0.25.0'
try {
    if( ! nextflow.version.matches(">= $nf_required_version") ){
        throw GroovyException('Nextflow version too old')
    }
} catch (all) {
    log.error "====================================================\n" +
              "  Nextflow version $nf_required_version required! You are running v$workflow.nextflow.version.\n" +
              "  Pipeline execution will continue, but things may break.\n" +
              "  Please run `nextflow self-update` to update Nextflow.\n" +
              "============================================================"
}







/*
 * STEP 1.1 - FastQC
 */
process fastqc {
	machineType 'n1-standard-1'
	tag "$prefix"
	publishDir "${params.outdir}/01-fastQC", mode: 'copy',
		saveAs: {filename -> filename.indexOf(".zip") > 0 ? "zips/$filename" : "$filename"}

	input:
	set val(name), file(reads) from raw_reads_fastqc

	output:
	file '*_fastqc.{zip,html}' into fastqc_results
	file '.command.out' into fastqc_stdout

	script:

	prefix = name - ~/(_S[0-9]{2})?(_L00[1-9])?(.R1)?(_1)?(_R1)?(_trimmed)?(_val_1)?(_00*)?(\.fq)?(\.fastq)?(\.gz)?$/
	"""
	mkdir tmp
	fastqc -t 4 -dir tmp $reads 
	rm -rf tmp
	"""
}

/*
 * STEPS 1.2 Trimming
 */
process trimming {
	machineType 'n1-standard-1'
	tag "$prefix"
	publishDir "${params.outdir}/02-preprocessing", mode: 'copy',
		saveAs: {filename ->
			if (filename.indexOf("_trimmed_fastqc") > 0) "03-postTrimQC/$filename"
			else if (filename.indexOf(".log") > 0) "logs/$filename"
      else if (params.saveTrimmed && filename.indexOf(".fastq.gz")) "trimmed/$filename"
			else null
	}

	input:
	set val(name), file(reads) from raw_reads_trimming
    file truseq1 from Channel.value(truseq)
	output:
	file '*.fastq.gz' into trimmed_reads,trimmed_reads_star

	file '*_fastqc.{zip,html}' into trimmomatic_fastqc_reports
	
    file '*.log' into trimmomatic_results

	script:
	prefix = name - ~/(_S[0-9]{2})?(_L00[1-9])?(.R1)?(_1)?(_R1)?(_trimmed)?(_val_1)?(_00*)?(\.fq)?(\.fastq)?(\.gz)?$/
	"""
    TrimmomaticSE -threads 4 $reads $prefix"_trimmed.fastq.gz"   ILLUMINACLIP:$truseq1:${params.trimmomatic_adapters_parameters} SLIDINGWINDOW:${params.trimmomatic_window_length}:${params.trimmomatic_window_value} MINLEN:${params.trimmomatic_mininum_length} 2> ${name}.log
    mkdir tmp
	fastqc -t 4 -q *.fastq.gz
	rm -rf tmp
	"""
}

process mapping_host {
	machineType 'e2-standard-16'
	tag "$prefix"
    label 'bigTask'

 publishDir "${params.outdir}/04-mapping", mode: 'copy',
		saveAs: {filename ->
			if (filename.indexOf("*Unmapped*") > 0) "Unmapped/$filename"
			else if (filename.indexOf(".out") > 0) "logs/$filename"
	}		

	input:
    file readsR from trimmed_reads_star
    file refhost from genome_idx_dir.collect()
  

	output:
	
    file "*.sam" into star_for_sam
    file "*.out"    into     star_results
    file "*Unmapped*" into u_bam_ch
   
	
    script:
	prefix = readsR.getName().replaceAll(/.fastq.gz/, "_")
    
    """
 STAR --runThreadN 16\
    --genomeDir $refhost \
    --readFilesCommand zcat \
    --readFilesIn ${readsR} \
    --outFilterType ${params.outFilterType}\
    --outReadsUnmapped ${params.outReadsUnmapped}\
    --outFilterMultimapNmax ${params.outFilterMultimapNmax}\
    --alignSJoverhangMin ${params.alignSJoverhangMin}\
    --alignSJDBoverhangMin ${params.alignSJDBoverhangMin}\
    --outFilterMismatchNmax ${params.outFilterMismatchNmax}\
    --outFilterMismatchNoverLmax ${params.outFilterMismatchNoverLmax}\
    --alignIntronMin ${params.alignIntronMin}\
    --alignIntronMax ${params.alignIntronMax}\
    --alignMatesGapMax ${params.alignMatesGapMax}\
    --outFilterScoreMinOverLread ${params.outFilterScoreMinOverLread}\
    --outFilterMatchNminOverLread ${params.outFilterMatchNminOverLread}\
    --outSAMattributes NH HI NM MD \
    --outFileNamePrefix ./${prefix}
	"""
}
process samtools {
	machineType 'n1-standard-1'
	tag "$prefix"
	publishDir "${params.outdir}/05-samtools", mode: 'copy',
		saveAs: {filename ->
			if (filename.indexOf("*sorted.bam") > 0) "sorted_bam/$filename"
			else if (filename.indexOf("*.txt") > 0) "logs/$filename"
	}		

	input:
	file sam from star_for_sam

	output:
	file '*sorted.bam' into samtools_results
    file '*.txt' into sam_flagstat

	script:

	prefix = sam.getName().replaceAll(/_Aligned.out.sam/, "_")
	"""
  
  samtools view -bS ${sam} > $prefix".bam"
  samtools sort $prefix".bam" -o $prefix"sorted.bam"
  samtools index $prefix"sorted.bam"
  samtools flagstat ./$prefix"sorted.bam" > ./$prefix"_flagstat.txt"
	
	"""
}

process featureCounts {
      label 'low_memory'
      tag "${bam_featurecounts.baseName - '.sorted'}"
      publishDir "${params.outdir}/06-featureCounts", mode: 'copy',
          saveAs: {filename ->
              if (filename.indexOf("biotype_counts") > 0) "biotype_counts/$filename"
              else if (filename.indexOf("_gene.featureCounts.txt.summary") > 0) "gene_count_summaries/$filename"
              else if (filename.indexOf("_gene.featureCounts.txt") > 0) "gene_counts/$filename"
              else "$filename"
          }

      input:
      file bam_featurecounts from samtools_results
      file gtf from gtf_featureCounts.collect()

      output:
      file "${bam_featurecounts.baseName}_unique_counts.txt" into geneCounts_unique, featureCounts_to_merge_unique
      file  "*_report.txt" 
      file "${bam_featurecounts.baseName}_multiple_counts.txt" into geneCounts_multiple, featureCounts_to_merge_multiple
      file "*.summary" into featureCounts_logs
      script:
     
      """
      featureCounts -T 1 -s ${params.stranded_lib} -O -a $gtf -t gene -g gene_id ${bam_featurecounts} -o ${bam_featurecounts.baseName}_unique_counts.txt 2> ${bam_featurecounts.baseName}_unique_counts_report.txt
      featureCounts  -T 1 -s ${params.stranded_lib} -M -O -a $gtf -t gene -g gene_id ${bam_featurecounts} -o ${bam_featurecounts.baseName}_multiple_counts.txt 2> ${bam_featurecounts.baseName}_multiple_counts_report.txt
      """}
    
      process merge_featureCounts {
      
      tag "${input_files[0].baseName - '.sorted'}"
      publishDir "${params.outdir}/06-featureCounts", mode: 'copy'

      input:
      file input_files from featureCounts_to_merge_unique.collect()

      output:
      file 'merged_gene_counts_unique.txt' into featurecounts_merged

      script:
      // Redirection (the `<()`) for the win!
      // Geneid in 1st column and gene_name in 7th
      gene_ids = "<(tail -n +2 ${input_files[0]} | cut -f1,7 )"
      counts = input_files.collect{filename ->
        // Remove first line and take third column
        "<(tail -n +2 ${filename} | sed 's:.sorted.bam::' | cut -f8)"}.join(" ")
      """
      paste $gene_ids $counts > merged_gene_counts_unique.txt
      """ 
}
    process multiqc {
	tag "$prefix"
    publishDir path: { "${params.outdir}/99-stats/MultiQC" }, mode: 'copy'

    input:
    file multiqc_config from multiqc_config
    file (fastqc:'fastqc/*') from fastqc_results.collect().ifEmpty([])
    file ('trimommatic/*') from trimmomatic_results.collect()
    file ('trimommatic/*') from trimmomatic_fastqc_reports.collect()
    file ('star/*') from star_results.collect()
    file ('featureCounts/*') from featureCounts_logs.collect()
    

    output:
    file '*multiqc_report.html' into multiqc_report
    file '*_data' into multiqc_data
    val prefix into multiqc_prefix

    script:
    prefix = fastqc[0].toString() - '_fastqc.html' - 'fastqc/'

    """
    multiqc -d . --config $multiqc_config
    """
}